﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Samochod:ISamochod
    {
        int liczbaMiejsc;
        public Samochod()
        {
            liczbaMiejsc = 4;
        }
        public void Swiec()
        {
            Console.WriteLine("Samochód świeci światłami i  ma {0} siedzeń", liczbaMiejsc);
        }

        public string Nazwa()
        {
            return "Samochód z " + liczbaMiejsc + " liczbą miejsc";
        }
    }
}
