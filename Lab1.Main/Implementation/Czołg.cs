﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Czołg: ICzolg
    {
        int liczbaNaboi;
        public Czołg()
        {
            liczbaNaboi = 2;
        }
        public void Strzelaj()
        {
            Console.WriteLine("Czołg strzela");
        }

        public string Nazwa()
        {
            return "Czołg z " + liczbaNaboi + "nabojami";
        }
    }
}
