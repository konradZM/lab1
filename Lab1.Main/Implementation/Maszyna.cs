﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Maszyna:ISamochod,IMaszyna
    {
        public string Nazwa()
        {
            return "Maszyna";
        }
        string IMaszyna.Nazwa()
        {
            return "Maszyna po maszynie";
        }
        void IMaszyna.Dzwiek()
        {
            Console.WriteLine("Wydaje dźwięk");
        }
        void ISamochod.Swiec()
        {
            Console.WriteLine("Swieci");
        }

        string IPojazd.Nazwa()
        {
            return "Maszyna po pojezdzie";
        }
    }
}
