﻿using System;
using Lab1.Contract;
using System.Collections.Generic;
using Lab1.Implementation;

namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
        public IList<IPojazd> zad2()
        {
            Czołg cz1 = new Czołg();
            Samochod s1 = new Samochod();
            Czołg cz2 = new Czołg();
            Samochod s2 = new Samochod();

            List<IPojazd> pojazdy = new List<IPojazd> { cz1, s1, cz2, s1 };
            return pojazdy;
        }
        public void Metoda(IList<IPojazd> Pojazdy)
        {
            foreach (var pojazd in Pojazdy)
            {
                Console.WriteLine(pojazd.Nazwa());
            }
        }
    }
}
