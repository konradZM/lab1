﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(IPojazd);
        
        public static Type ISub1 = typeof(ISamochod);
        public static Type Impl1 = typeof(Samochod);
        
        public static Type ISub2 = typeof(ICzolg);
        public static Type Impl2 = typeof(Czołg);
        
        
        public static string baseMethod = "Nazwa";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Swiec";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Strzelaj";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "zad2";
        public static string collectionConsumerMethod = "Metoda";

        #endregion

        #region P3

        public static Type IOther = typeof(IMaszyna);
        public static Type Impl3 = typeof(Maszyna);

        public static string otherCommonMethod = "Nazwa";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
